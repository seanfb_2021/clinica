/* ......................................................................... */
/* js_ChangeEstilo: cambia el estilo y deja readonly, text's un formulario   */
/* ......................................................................... */

var jsBgColor      = '#ffffff';
var jsColor        = '#000099';
var jsFontWeight   = 'normal';
var jsBorderLeft   = '0px solid #e0e0e0';
var jsBorderRight  = '0px solid #a0a0a0';
var jsBorderTop    = '0px solid #e0e0e0';
var jsBorderBottom = '0px solid #a0a0a0';
var jsPaddingLeft  = '0.2em';
var jsReadOnly     = true;

function js_ChangeEstilo ( formobj )
{
   for ( var i = 0; i < formobj.elements.length; i++ ){

      var obj = formobj.elements[i];

      if ( obj ){
         if ( obj.type == "text" ){
            obj.style.backgroundColor = jsBgColor;
            obj.style.color           = jsColor;
            obj.style.fontWeight      = jsFontWeight;
            obj.style.borderLeft      = jsBorderLeft;
            obj.style.borderRight     = jsBorderRight;
            obj.style.borderTop       = jsBorderTop;
            obj.style.borderBottom    = jsBorderBottom;
            obj.style.paddingLeft     = jsPaddingLeft;
            obj.readOnly              = jsReadOnly;
         }
      }

   }
}

function js_changeSHWEstilo ( formobj )
{
   for ( var i = 0; i < formobj.elements.length; i++ ){

      var obj = formobj.elements[i];

      if ( obj ){
         if ( objName = obj.name ){
         if ( objName.substring(0, 3) == "shw" ){
            obj.style.backgroundColor = jsBgColor;
            obj.style.color           = jsColor;
            obj.style.fontWeight      = jsFontWeight;
            obj.style.borderLeft      = jsBorderLeft;
            obj.style.borderRight     = jsBorderRight;
            obj.style.borderTop       = jsBorderTop;
            obj.style.borderBottom    = jsBorderBottom;
            obj.style.paddingLeft     = jsPaddingLeft;
            obj.readOnly              = jsReadOnly;
         }
         }
      }

   }
}

