
function formCheck ( formobj, fieldRequired, fieldDescription, chequear )
{

   var alertMsg = "ADVERTENCIA:\n\nPor favor, completar los siguientes " +
                  "campos obligatorios:\n\n";
   var ischecked = chequear;
   var l_Msg = alertMsg.length;

   for ( var i = 0; i < fieldRequired.length; i++ ){

      var obj = formobj.elements[fieldRequired[i]];

      if ( obj ) {
         switch ( obj.type ) {
            case "select-one":
                if ( obj.selectedIndex == -1 ||
                     obj.options[obj.selectedIndex].value == -1 ||
                     obj.options[obj.selectedIndex].text == "")
                {
                     alertMsg += " - " + fieldDescription[i] + "\n";
                }
                break;

            case "select-multiple":
                   if ( obj.selectedIndex == -1 ){
                       alertMsg += " - " + fieldDescription[i] + "\n";
                   }
                   break;

            case "text":
            case "textarea":
                  if ( obj.value == "" || obj.value == null ){
                      alertMsg += " - " + fieldDescription[i] + "\n";
                  }
                  break;

            /**** falta por lo menos algun checkbox ***/
            case "checkbox":
                if ( ! ischecked ) {
                   ischecked = ( ischecked || obj.checked );
                   var posi  = i;
                }
                break;

            default:
         }

         if ( obj.type == undefined ){
            var blnchecked = false;
            for ( var j = 0; j < obj.length; j++ ){
               if ( obj[j].checked ) {
                  blnchecked = true;
               }
            }
         
            if ( !blnchecked ){
               alertMsg += " - " + fieldDescription[i] + "\n";
            }
         }

      } /* end if obj */

   } /* end for */

   if ( ! ischecked ) {
      alertMsg += " - " + fieldDescription[posi] + "\n";
   }

   if ( alertMsg.length == l_Msg ){
        return true;
   }
   else{
        alert ( alertMsg );
        return false;
   }
}
