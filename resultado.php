<?php

    include("biblioteca/php/init.php");
    include ("cabecera.php");
		include ("menu.php");
		include ("pie.php");
		include ("construccion.php");

    $HOS = "172.31.16.144"; 
    $USU = "clinica";
    $PAS = "Arquitectura.2021";
    $BDD = "comunity_clinica" ;   
    $db = new mysqli($HOS, $USU, $PAS , $BDD);
    mysqli_set_charset( $db, 'utf8');

    if ($db->connect_error)
    	die('Error de Conexion ('.$db->connect_errno.')'.$db->connect_error);
      
    $query = "SELECT CONCAT(P.CLPAC_NOMBRE,' ', P.CLPAC_APELLIDO) AS NOMBRE_PACIENTE 
      						   	  ,DATE_FORMAT(A.CLATE_FECHA_MUESTRA, '%d-%m-%Y') AS FECHA_MUESTRA
      						   	  ,DATE_FORMAT(A.CLATE_FECHA_RESULTADO, '%d-%m-%Y') AS FECHA_RESULTADO
     							  ,E.CLEST_GLOSA AS ESTADO_EXAMEN
    							  ,R.CLESR_GLOSA AS RESULTADO
    							  ,T.CLTIP_GLOSA AS EXAMEN
    							  ,A.CLATE_OBSERVACION AS OBSERVACION
							FROM   CL_PACIENTE P INNER JOIN CL_ATENCION A ON P.CLPAC_ID = A.CLPAC_ID,
       							 CL_ESTADO_EXAMEN E,
       							 CL_ESTADO_RESULTADO R,
       							 CL_TIPO_EXAMEN T
							WHERE  A.CLEST_ID = E.CLEST_ID      
							AND    A.CLESR_ID = R.CLESR_ID
							AND    A.CLTIP_ID = T.CLTIP_ID
							AND    P.CLPAC_RUT = '".$_POST["rutUsuario"]."'
							AND    A.CLATE_ID = ".$_POST["idAtencion"]; 
							
		//echo $query;
		//return;					
							
    $res = $db->query($query);
    
    $numfilas = $res->num_rows;

    if ( $numfilas > 0 ) {
    	$row = $res->fetch_object();
    }

?>

<html>
	
<head>	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php echo $GLOBALS['TITULO'] ?></title>
	<link rel="icon" href="<?php echo $GLOBALS['DIR_IMG'] ?>favicon.ico">
	<link rel="stylesheet" href="<?php echo $GLOBALS['DIR_CSS'] ?>css.css" />
</head>

<body style="background-color: #F5F5F5;">
<article>
   <form name="frmResultado" method="POST" action="index.php">

   <div style="margin-left: 200px; margin-top: 100px; font-size: 16px;">	
  	<div><b>RESULTADO CONSULTA DE EXAMENES EN LINEA</b></div>
  	<br>
  	
    <div>Informante: LABORATORIO CLÍNICO</div>		
  	<br>
  	
    <div>Examen: <?php echo $row->EXAMEN ?></div> 	
  	<br>
  	
  	<div>
	  	<table class="tbl" width="90%" align="left">
				<tr>
	  		  	  <td style="background-color: #000080; color: white;" align="center">Nombre Paciente</td>
				  		<td style="background-color: #000080; color: white;" align="center">Fecha Toma de Muestra</td>
				  		<td style="background-color: #000080; color: white;" align="center">Fecha Resultado</td>
				  		<td style="background-color: #000080; color: white;" align="center">Estado Examen</td>
				  		<td style="background-color: #000080; color: white;" align="center">Resultado</td>
		  	</tr>
	<?php 
	   if ( $numfilas == 0 ) {
	?>  	
	    <tr>
	    	<td colspan="5">No se encontraron resultados...</td>
	    </tr>
	<?php 
	   }else{
	    	
	?>  	
	   
	    <tr class="tbl-row tbl-row-even">
	    	<td class="tbl-cell" align="left"><?php echo $row->NOMBRE_PACIENTE ?></td>
	    	<td class="tbl-cell" align="center"><?php echo $row->FECHA_MUESTRA ?></td>
	    	<td class="tbl-cell" align="center"><?php echo $row->FECHA_RESULTADO ?></td>
	    	<td class="tbl-cell" align="left"><?php echo $row->ESTADO_EXAMEN ?></td>
	    	<td class="tbl-cell" align="left"><?php echo $row->RESULTADO ?></td>
	    </tr>

	    <tr height="30"><td colspan="5"></td></tr> 
	   
	    <tr>
	  	  <td align="left" colspan="5">Indicaciones:</td>
	  	</tr>
	  	<tr>
	  	  <td align="left" colspan="5">OBS:<?php echo $row->OBSERVACION ?></td>
	  	</tr>
	  	
	    <tr height="30"><td colspan="5"></td></tr> 
	  	<tr height="30"><td colspan="5"></td></tr> 
	  	
	  
<?php } ?>

	<tr> <td colspan="5" align="center"> 
 	        <input type="submit" name="volver" value="Volver a Consulta Principal" id="boton_volver">
            </td>
	  	</tr>
	  	
	  	
	    </table>
    </div>

   </div>
    
   </br></br> 
    
  </form>

</article>

</html>
