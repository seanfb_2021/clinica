<?php
//error_reporting(E_ALL ^ E_NOTICE);

//session_cache_limiter('nocache');
@session_start();

/* ......................................................................... */
/* definicion de variables globales                                          */
/* ......................................................................... */
$MODO_PRUEBA = "false";
$MAX_LINEAS_DTE = 24;
//$HOST     = "190.98.227.173";
//$HOST     = "127.0.0.1";

$HOST     = "172.31.16.144"; 


/* ......................................................................... */
/* defino el nombre y la clave que viene de la autentificacion de apache     */
/* ......................................................................... */
$HOST     = "172.31.16.144"; 
$USUARIO     = "clinica";
$PASSWORD    = "Arquitectura.2021";
$BD  = "comunity_clinica" ;     /* la base de datos de adm. cliente     */

date_default_timezone_set('America/Santiago');
//date_default_timezone_set('America/Argentina/Buenos_Aires');

/* ......................................................................... */
/* establezco la conexion con el servidor y la bbdd mysql                    */
/* ......................................................................... */
if(!($CONEXION = mysqli_connect($HOST, $USUARIO, $PASSWORD, $BD))){
	echo "ERROR DE CONEXION N&ordm; = ".mysql_errno();
	exit();
}
//mysql_select_db($BD, $CONEXION) or  die('ERROR: NO SE ENCUENTRA LA BASE DE DATOS');
mysqli_set_charset( $CONEXION, 'utf8');
@mysqli_query( $CONEXION, "SET NAMES 'utf8'");  // Mantiene coherencia palabras con acento al interior BBDD


/* ......................................................................... */
/* directorios que se enlazan de forma virtual al estar el apache arriba     */
/* ......................................................................... */
$GLOBALS['TITULO']       = "CLINICA PURA VIDA";
$DIR_BIBLIO              = "../biblioteca";
$GLOBALS['DIR_JS']       = $DIR_BIBLIO ."/js/";
$GLOBALS['DIR_CSS']      = $DIR_BIBLIO ."/css/";
$GLOBALS['DIR_PHP']      = $DIR_BIBLIO ."/php/";
$GLOBALS['DIR_IMG']      = $DIR_BIBLIO ."/imagenes/";


$hoy = date("Y-m-d");
$fecha_actual  = date("Y-m-d H:i:s");

$ACCESO_MENUS;
$ACCESO_OPCIONES;


/* ......................................................................... */
/* funciones que se encargan de imprimir los valores css y js                */
/* ......................................................................... */
function printEnlaces ( $putjs = true )
{
   echo "<link rel=\"stylesheet\" href=\"".$GLOBALS['DIR_CSS']."botones.css\" type=\"text/css\">\n\n";
   echo "<link rel=\"stylesheet\" href=\"".$GLOBALS['DIR_CSS']."moto.css\" type=\"text/css\">\n\n";
   echo "<link rel=\"stylesheet\" href=\"".$GLOBALS['DIR_CSS']."listajax.css\" type=\"text/css\">\n\n";

   if ( $putjs ) {
      echo "<script language=\"JavaScript\" src=\"".$GLOBALS['DIR_JS']."funciones.js\"></script>\n";
      //echo "<script language=\"JavaScript\" src=\"".$GLOBALS['DIR_JS']."js_estilo.js\"></script>\n";
      echo "<script language=\"JavaScript\" src=\"".$GLOBALS['DIR_JS']."valform.js\"></script>\n";
      echo "<script language=\"JavaScript\" src=\"".$GLOBALS['DIR_JS']."calendario.js\"></script>\n\n";
   }
   return ;
}
?>
